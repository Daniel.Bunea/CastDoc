all: server.c client.c
	gcc -o server.exe inc/*.c inc/server/*.c server.c -l pthread
	gcc -o client.exe inc/*.c inc/client/*.c client.c -l pthread
	
clean:
	rm client.exe server.exe