cmake_minimum_required(VERSION 3.6)
project(castDoc)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        inc/client/helpers.c
        inc/client/helpers.h
        inc/server/helpers.c
        inc/server/helpers.h
        inc/shared.c
        inc/shared.h
        client.c
        server.c)

add_executable(castDoc ${SOURCE_FILES})